﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SongView : MonoBehaviour
{
    [HideInInspector] public int ID;
    public Text title;
    public Text singer;
    public Text price;
    public Text percent;
    public Toggle toggle;
    public GameObject buy;

    public void Initialize(int ID, string title, string singer, int price, float percent, int isUnlocked)
    {
        this.title.text = title.ToUpper();
        this.singer.text = singer.ToUpper();
        this.price.text = price.ToString();
        this.toggle.interactable = isUnlocked == 1 ? true : false;
        this.percent.gameObject.SetActive(this.toggle.interactable);
        this.percent.text = (percent * 100) + "%";
        this.buy.SetActive(!this.toggle.interactable);
    }
}
