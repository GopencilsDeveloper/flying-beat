﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SonicBloom.Koreo;
using NaughtyAttributes;

[System.Serializable]
public class Song : Item
{
    public string singer;
    [HideInInspector] public float percent;
    public Koreography koreography;
}
[System.Serializable]
public class Effect : Item
{

}
[System.Serializable]
public class Theme : Item
{
}

public class ItemsManager : MonoSingleton<ItemsManager>
{
    #region CONST
    const string SONG = "SONG";
    const string ISUNLOCKED = "isUnlocked";
    #endregion

    #region SONG
    [Header("Song")]
    public GameObject songPrefab;
    public Transform songListView;
    public List<Song> songList;
    #endregion

    #region EFFECT
    [Header("Effect")]
    public List<Song> effectList;
    #endregion

    #region THEME
    [Header("Theme")]
    public List<Song> themeList;
    #endregion

    public override void Initialize()
    {
        foreach (var song in songList)
        {
            var newSong = Instantiate(songPrefab) as GameObject;
            SongView songView = newSong.GetComponent<SongView>();
            
            if (PlayerPrefs.GetInt(SONG + song.ID + ISUNLOCKED) == 1)
            {
                song.isUnlocked = 1;
            }

            songView.Initialize(song.ID, song.title, song.singer, song.price, song.percent, song.isUnlocked);
            newSong.transform.SetParent(songListView);
        }
    }

    void UpdateData()
    {
        GameObject[] songs = GameObject.FindGameObjectsWithTag("Song");
        foreach (var song in songs)
        {
            SongView songView = song.GetComponent<SongView>();
        }
    }

    void DeleteAll()
    {
        PlayerPrefs.DeleteAll();
    }

    [Button]
    private void UnlockSong()
    {
        PlayerPrefs.SetInt(SONG + "2" + ISUNLOCKED, 1);
    }
    [Button]
    private void LockSong()
    {
        PlayerPrefs.SetInt(SONG + "2" + ISUNLOCKED, 0);
    }
}
