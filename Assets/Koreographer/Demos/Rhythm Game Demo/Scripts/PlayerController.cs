﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class PlayerController : MonoBehaviour
{
    private Vector3 pos;
    private float minY;
    private float maxY;
    private Rigidbody2D rgb;

    /* [ShowNonSerializedField] private float speed;
    [SerializeField] private float maxSpeed = 15;
    [SerializeField] private float acceleration = 20;
    [SerializeField] private float deceleration = 10; */

    void Initialize()
    {
        rgb = GetComponent<Rigidbody2D>();
        pos = transform.position;
        GetPlayerRangeY();
    }

    void Start()
    {
        Initialize();
    }

    void Update()
    {
/*         #if UNITY_EDITOR
                if (Input.GetKey(KeyCode.DownArrow) && (speed < maxSpeed) && (speed > -maxSpeed))
                {
                    speed -= acceleration * Time.deltaTime;
                }
                else if (Input.GetKey(KeyCode.UpArrow) && (speed < maxSpeed) && (speed > -maxSpeed))
                {
                    speed += acceleration * Time.deltaTime;
                }
                else
                {
                    if (speed > deceleration * Time.deltaTime)
                        speed -= deceleration * Time.deltaTime;
                    else if (speed < -deceleration * Time.deltaTime)
                        speed += deceleration * Time.deltaTime;
                    else
                        speed -= deceleration * 10 * Time.deltaTime;
                }
        #else
                if (Input.touchCount > 0)
                {
                    Touch touch = Input.GetTouch(0);
                    if (touch.phase == TouchPhase.Moved)
                    {
                        if (touch.deltaPosition.y < 0 && (speed < maxSpeed) && (speed > -maxSpeed))
                        {
                            speed -= acceleration * Time.deltaTime;
                        }
                        else if (touch.deltaPosition.y > 0  && (speed < maxSpeed) && (speed > -maxSpeed))
                        {
                            speed += acceleration * Time.deltaTime;
                        }
                        else
                        {
                            if (speed > deceleration * Time.deltaTime)
                                speed -= deceleration * Time.deltaTime;
                            else if (speed < -deceleration * Time.deltaTime)
                                speed += deceleration * Time.deltaTime;
                            else
                                speed = 0;
                        }
                    }
                }
        #endif */

/* #if UNITY_EDITOR
        if (Input.GetKey(KeyCode.DownArrow))
        {
            pos.y -= 5 * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            pos.y += 5 * Time.deltaTime;
        }
#else
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            Touch touch = Input.GetTouch(0);
            pos.y += touch.deltaPosition.y / 3 * Time.deltaTime;
        }
#endif

        // pos.y += speed * Time.deltaTime;
        pos.y = Mathf.Clamp(pos.y, minY, maxY);
        transform.position = pos; */

        if (Input.GetMouseButtonDown(0))
        {
            rgb.velocity = new Vector2(0, -20);
        }
    }

    private void GetPlayerRangeY()
    {
        float cameraOffsetZ = -Camera.main.transform.position.z;
        maxY = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, cameraOffsetZ)).y - 2f;
        minY = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, cameraOffsetZ)).y + 2f;
    }

    int score = 0;
    public UnityEngine.UI.Text scoreTxt;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Note")
        {
            print("note");
            rgb.velocity = Vector2.zero;
            rgb.AddForce(new Vector2(0, 150));
        }
    }

}
